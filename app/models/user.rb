class User < ActiveRecord::Base
  has_many :jobs, class_name: 'Job', foreign_key: :owner
end
