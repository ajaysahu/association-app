json.array!(@jobs) do |job|
  json.extract! job, :id, :code, :owner
  json.url job_url(job, format: :json)
end
