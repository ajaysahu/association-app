class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :code
      t.integer :owner

      t.timestamps null: false
    end
  end
end
